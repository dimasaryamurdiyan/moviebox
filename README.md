# Movie Box App

This is the guide about how this app was developed
The app was developed with latest modern android development :
- ***Kotlin***
- ***Modularization***
- ***Clean Architecture pattern***
- ***Hilt - Dependency Injection***
- ***Coroutines***
- ***Flow***
- ***Paging3***
- ***Version Catalog***
- ***pierfrancescosoffritti - youtube player***
- ***Android Min SDK: 21***
- ***Android Target SDK: 33***


## Android App Architecture - Guideline to Modularization
![movie-gif](./assets/gofood-clone-Page-3.drawio.png)

**Top tip**: A module graph (shown above) can be useful during modularization planning for
visualizing dependencies between modules.

The MovieBox app contains the following types of modules:

* The `app` module - contains app level, presentation layer and scaffolding classes that bind the rest of the codebase,
  such as `MainActivity`, `MyApplication` and dependency injection. The `app` module depends on `data` module and
  required `domain` module.

* `data:` module - contains operation about accessing or manipulating data both network nor local.
  This module depend on `domain` module, but it shouldn’t depend on app module.

* `domain:` module - most deep layer that contains all business logic for each specific usecase, such as `GetPopularMovieUseCase`, `GetDetailMovieUseCase`, etc. This module not depend to any module, because this is the deepest layer based on Clean Architecture guidelines.


## Main Features
- Show all movie genre
- Show all popular movie
- Filter movie by genre selected
- Show detail movie
- Show youtube trailer
- Pagination on movie list and review list

## Demo :)


<img src="https://gitlab.com/dimasaryamurdiyan/moviebox/-/raw/main/assets/moviebox.gif" width="45%" size="auto" alt="weka">




## Credits
- **Dimas Arya Murdiyan** - dimasaryamurdiyan123@gmail.com
- [TMDB API - Free Movie API](https://developer.themoviedb.org/reference/intro/getting-started)