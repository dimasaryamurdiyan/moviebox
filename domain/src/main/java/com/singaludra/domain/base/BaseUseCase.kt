package com.singaludra.domain.base

/**
 * @param P
 * @return a Resource written in R
* */
abstract class BaseUseCase<in P, R> {
    /**
     * Override this to set the code to be executed.
     */
    @Throws(RuntimeException::class)
    abstract fun execute(parameters: P): R

}