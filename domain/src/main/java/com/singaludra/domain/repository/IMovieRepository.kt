package com.singaludra.domain.repository

import androidx.paging.PagingData
import com.singaludra.domain.Resource
import com.singaludra.domain.model.Movie
import com.singaludra.domain.model.Review
import com.singaludra.domain.model.Video
import kotlinx.coroutines.flow.Flow

interface IMovieRepository {
    fun getMovies(genreId: String?): Flow<PagingData<Movie>>
    fun getMovieGenre(): Flow<Resource<List<Movie.Genre>>>
    fun getDetailMovie(id: Int): Flow<Resource<Movie>>
    fun getMovieReviews(id: Int): Flow<PagingData<Review>>
    fun getMovieVideos(id: Int): Flow<Resource<List<Video>>>
}