package com.singaludra.domain.usecases

import com.singaludra.domain.Resource
import com.singaludra.domain.base.BaseUseCase
import com.singaludra.domain.model.Movie
import com.singaludra.domain.repository.IMovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetDetailMovieUseCase @Inject constructor(
    private val movieRepository: IMovieRepository
): BaseUseCase<Int, Flow<Resource<Movie>>>() {
    override fun execute(parameters: Int): Flow<Resource<Movie>> {
        return movieRepository.getDetailMovie(parameters)
    }
}