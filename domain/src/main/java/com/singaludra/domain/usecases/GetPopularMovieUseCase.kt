package com.singaludra.domain.usecases

import androidx.paging.PagingData
import com.singaludra.domain.base.BaseUseCase
import com.singaludra.domain.model.Movie
import com.singaludra.domain.repository.IMovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPopularMovieUseCase @Inject constructor(
    private val repository: IMovieRepository
): BaseUseCase<String?, Flow<PagingData<Movie>>>() {
    override fun execute(parameters: String?): Flow<PagingData<Movie>>{
        return repository.getMovies(parameters)
    }
}