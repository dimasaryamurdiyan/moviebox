package com.singaludra.domain.usecases

import com.singaludra.domain.Resource
import com.singaludra.domain.base.BaseUseCase
import com.singaludra.domain.repository.IMovieRepository
import com.singaludra.domain.model.Video
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovieVideosUseCase @Inject constructor(
    private val movieRepository: IMovieRepository
): BaseUseCase<Int, Flow<Resource<List<Video>>>>() {
    override fun execute(parameters: Int): Flow<Resource<List<Video>>> {
        return movieRepository.getMovieVideos(parameters)
    }
}

