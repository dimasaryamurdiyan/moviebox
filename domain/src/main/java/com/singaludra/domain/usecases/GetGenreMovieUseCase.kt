package com.singaludra.domain.usecases

import com.singaludra.domain.base.BaseUseCase
import com.singaludra.domain.repository.IMovieRepository
import com.singaludra.domain.Resource
import com.singaludra.domain.model.Movie
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetGenreMovieUseCase @Inject constructor(
    private val repository: IMovieRepository
): BaseUseCase<Unit, Flow<Resource<List<Movie.Genre>>>>() {
    override fun execute(parameters: Unit): Flow<Resource<List<Movie.Genre>>> {
        return repository.getMovieGenre()
    }
}