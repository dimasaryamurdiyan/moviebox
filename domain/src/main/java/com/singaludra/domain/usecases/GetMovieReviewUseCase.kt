package com.singaludra.domain.usecases

import androidx.paging.PagingData
import com.singaludra.domain.base.BaseUseCase
import com.singaludra.domain.model.Review
import com.singaludra.domain.repository.IMovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovieReviewUseCase @Inject constructor(
    private val repository: IMovieRepository
): BaseUseCase<Int, Flow<PagingData<Review>>>() {
    override fun execute(parameters: Int): Flow<PagingData<Review>> {
        return repository.getMovieReviews(parameters)
    }
}