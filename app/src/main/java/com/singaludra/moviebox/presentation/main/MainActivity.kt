package com.singaludra.moviebox.presentation.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import com.singaludra.domain.model.Movie
import com.singaludra.moviebox.databinding.ActivityMainBinding
import com.singaludra.moviebox.presentation.detail.DetailActivity
import com.singaludra.moviebox.presentation.main.adapter.GenreAdapter
import com.singaludra.moviebox.presentation.main.adapter.MovieAdapter
import com.singaludra.moviebox.presentation.main.adapter.MovieLoadStateAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var movieAdapter: MovieAdapter
    private lateinit var genreAdapter: GenreAdapter

    private val viewModel by viewModels<MainViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        onViewBind()
        fetchViewModelCallback()
        onViewObserve()
    }

    private fun fetchViewModelCallback(genreId: String? = null){
        viewModel.apply {
            lifecycleScope.launch {
                getMovies(genreId).collectLatest { movies ->
                    movieAdapter.submitData(movies)
                }
            }
        }
    }

    private fun onViewObserve() {
        viewModel.apply {
            genre.observe(this@MainActivity){ resource ->
                when (resource) {
                    is com.singaludra.domain.Resource.Loading -> {
                        // Handle loading state (e.g., show a progress bar)
                    }
                    is com.singaludra.domain.Resource.Success -> {
                        // Handle success state (e.g., update UI with data)
                        val genres = resource.data
                        setupGenreFilter(genres)
                        // Update your UI with the genre data
                    }
                    is com.singaludra.domain.Resource.Error -> {
                        // Handle error state (e.g., show an error message)
                        val errorMessage = resource.message
                        // Handle the error
                    }
                }
            }
        }
    }

    private fun setupGenreFilter(genres: List<Movie.Genre>?) {
        val genreList = genres?.toMutableList()
        genreList?.add(0, Movie.Genre(id = 0, name = "All"))
        genreAdapter = GenreAdapter(object : GenreAdapter.OnClickListener{
            override fun filter(courseId: Int?, selectedItem: Int?, position: Int?) {
                if (courseId == 0) {
                    fetchViewModelCallback()
                } else {
                    fetchViewModelCallback(courseId.toString())
                }
            }

        })
        genreAdapter.differ.submitList(genreList)
        binding.rvMovieGenre.adapter = genreAdapter
    }

    private fun onViewBind() {
        binding.apply {
            movieAdapter = MovieAdapter(object : MovieAdapter.OnClickListener {
                override fun onClickItem(item: Movie) {
                    val intent = Intent(this@MainActivity, DetailActivity::class.java)
                    intent.putExtra(DetailActivity.EXTRA_DATA, item.id)
                    startActivity(intent)
                }
            })

            rvMovies.adapter = movieAdapter.withLoadStateHeaderAndFooter(
                header = MovieLoadStateAdapter{movieAdapter.retry()},
                footer = MovieLoadStateAdapter{movieAdapter.retry()}
            )

            movieAdapter.addLoadStateListener { loadState -> renderUi(loadState) }

            binding.btnMoviesRetry.setOnClickListener { movieAdapter.retry() }
        }
    }

    private fun renderUi(loadState: CombinedLoadStates) {
        val isListEmpty = loadState.refresh is LoadState.NotLoading && movieAdapter.itemCount == 0

        binding.rvMovies.isVisible = !isListEmpty
        binding.tvMoviesEmpty.isVisible = isListEmpty

        // Only shows the list if refresh succeeds.
        binding.rvMovies.isVisible = loadState.source.refresh is LoadState.NotLoading
        // Show loading spinner during initial load or refresh.
        binding.progressBarMovies.isVisible = loadState.source.refresh is LoadState.Loading
        // Show the retry state if initial load or refresh fails.
        binding.btnMoviesRetry.isVisible = loadState.source.refresh is LoadState.Error
    }
}