package com.singaludra.moviebox.presentation.main.adapter

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.singaludra.domain.model.Movie
import com.singaludra.moviebox.R
import com.singaludra.moviebox.databinding.ItemMovieGenreBinding

class GenreAdapter(private val itemClick: OnClickListener): RecyclerView.Adapter<GenreAdapter.ViewHolder>()  {
    inner class ViewHolder(private val binding: ItemMovieGenreBinding) : RecyclerView.ViewHolder(binding.root) {
        @RequiresApi(Build.VERSION_CODES.M)
        fun bind(item: Movie.Genre, position: Int){
            binding.apply {
                tvTopicTitle.text = item.name
                if (selectedItem == position) {
                    root.setBackgroundResource(R.drawable.bg_chip_selected)
                    tvTopicTitle.apply {
                        setTextColor(context.getColor(R.color.white))
                        setTypeface(tvTopicTitle.typeface, (Typeface.BOLD))
                    }
                } else {
                    root.setBackgroundResource(R.drawable.bg_chip_unselected)
                    tvTopicTitle.apply {
                        setTextColor(context.getColor(R.color.black))
                        setTypeface(tvTopicTitle.typeface, (Typeface.NORMAL))
                    }
                }

                root.setOnClickListener {
                    if (selectedItem != position) {
                        selectedItem = position
                        itemClick.filter(item?.id,selectedItem, position)
                        notifyItemChanged(position)
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GenreAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            ItemMovieGenreBinding.inflate(
                inflater,
                parent,
                false
            )
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(
        holder: GenreAdapter.ViewHolder,
        position: Int
    ) {
        val item = differ.currentList[position]
        holder.bind(item, position)
        holder.setIsRecyclable(false)
    }

    override fun getItemCount()= differ.currentList.size

    private val differCallback = object : DiffUtil.ItemCallback<Movie.Genre>(){
        override fun areItemsTheSame(oldItem: Movie.Genre, newItem: Movie.Genre): Boolean {
            return  oldItem.id == newItem.id
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Movie.Genre, newItem: Movie.Genre): Boolean {
            return oldItem == newItem
        }

    }

    private var selectedItem = 0
    val differ = AsyncListDiffer(this,differCallback)

    interface OnClickListener {
        fun filter(courseId: Int?, selectedItem: Int?, position: Int?)
    }
}