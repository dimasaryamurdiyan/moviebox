package com.singaludra.moviebox.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.singaludra.domain.Resource
import com.singaludra.domain.model.Movie
import com.singaludra.domain.usecases.GetGenreMovieUseCase
import com.singaludra.domain.usecases.GetPopularMovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getPopularMovieUseCase: GetPopularMovieUseCase,
    private val getGenreMovieUseCase: GetGenreMovieUseCase
): ViewModel() {
    private val _genre = MutableLiveData<Resource<List<Movie.Genre>>>(
        Resource.Loading()
    )
    val genre: LiveData<Resource<List<Movie.Genre>>> get() = _genre

    init {
        getMovieGenre()
    }

    private fun getMovieGenre(){
        viewModelScope.launch {
            getGenreMovieUseCase.execute(Unit).collect{
                _genre.postValue(it)
            }
        }
    }

    fun getMovies(genreId: String?): Flow<PagingData<Movie>> {
        return getPopularMovieUseCase.execute(genreId)
            .cachedIn(viewModelScope)
    }
}