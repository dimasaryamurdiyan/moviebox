package com.singaludra.moviebox.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.singaludra.domain.Resource
import com.singaludra.domain.model.Movie
import com.singaludra.domain.model.Review
import com.singaludra.domain.model.Video
import com.singaludra.domain.usecases.GetDetailMovieUseCase
import com.singaludra.domain.usecases.GetMovieReviewUseCase
import com.singaludra.domain.usecases.GetMovieVideosUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val getDetailMovieUseCase: GetDetailMovieUseCase,
    private val getMovieReviewUseCase: GetMovieReviewUseCase,
    private val getMovieVideosUseCase: GetMovieVideosUseCase
): ViewModel() {
    private val _detailMovie = MutableLiveData<Resource<Movie>>()
    val detailMovie: LiveData<Resource<Movie>> get() = _detailMovie

    fun getDetailGame(id: Int) {
        viewModelScope.launch {
            getDetailMovieUseCase.execute(id).collect{
                _detailMovie.postValue(it)
            }
        }
    }

    private val _videoList = MutableLiveData<Resource<List<Video>>>()
    val videoList: LiveData<Resource<List<Video>>> get() = _videoList

    fun getMovieVideos(id: Int) {
        viewModelScope.launch {
            getMovieVideosUseCase.execute(id).collect{
                _videoList.postValue(it)
            }
        }
    }

    fun getMovieReview(id: Int): Flow<PagingData<Review>> {
        return getMovieReviewUseCase.execute(id)
            .cachedIn(viewModelScope)
    }
}