package com.singaludra.moviebox.presentation.detail

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import com.singaludra.moviebox.BuildConfig
import com.singaludra.domain.Resource
import com.singaludra.domain.model.Movie
import com.singaludra.moviebox.databinding.ActivityDetailBinding
import com.singaludra.moviebox.databinding.ViewGenreChipBinding
import com.singaludra.moviebox.presentation.common.VideoPlayerDialogFragment
import com.singaludra.moviebox.presentation.detail.adapter.ReviewAdapter
import com.singaludra.moviebox.presentation.main.adapter.MovieLoadStateAdapter
import com.singaludra.moviebox.utils.Utils
import com.singaludra.moviebox.utils.hourMinutes
import com.singaludra.moviebox.utils.loadImage
import com.singaludra.moviebox.utils.roundTo
import com.singaludra.moviebox.utils.shortToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    private val viewModel by viewModels<DetailViewModel>()

    private val movieId by lazy {
        intent.getIntExtra(EXTRA_DATA, 0)
    }

    private val reviewAdapter by lazy {
        ReviewAdapter()
    }

    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fetchViewModelCallback()
        onViewBind()
        onViewObserve()
    }

    private fun onViewBind() {
        binding.apply {
            rvReview.apply {
                layoutManager = LinearLayoutManager(this@DetailActivity, LinearLayoutManager.HORIZONTAL ,false)
                adapter = reviewAdapter.withLoadStateHeaderAndFooter(
                    header = MovieLoadStateAdapter{reviewAdapter.retry()},
                    footer = MovieLoadStateAdapter{reviewAdapter.retry()}
                )

                reviewAdapter.addLoadStateListener { loadState -> setupReviewMovie(loadState) }
            }
            icBackButton.setOnClickListener {
                finish()
            }
        }
    }

    private fun setupReviewMovie(loadState: CombinedLoadStates) {
        val isListEmpty = loadState.refresh is LoadState.NotLoading && reviewAdapter.itemCount == 0

        binding.rvReview.isVisible = !isListEmpty
        binding.tvEmpty.isVisible = isListEmpty

        // Only shows the list if refresh succeeds.
        binding.rvReview.isVisible = loadState.source.refresh is LoadState.NotLoading
        // Show the retry state if initial load or refresh fails.
        binding.btnRetry.isVisible = loadState.source.refresh is LoadState.Error
    }

    private fun onViewObserve() {
        viewModel.apply{
            detailMovie.observe(this@DetailActivity){
                when(it){
                    is Resource.Success -> {
                        setupDetailMovie(it.data)
                        hideLoading()
                    }
                    is Resource.Error -> {
                        hideLoading()
                        this@DetailActivity.shortToast(it.message ?: "Something went wrong")
                    }
                    is Resource.Loading -> {
                        showLoading()
                    }
                }
            }
            videoList.observe(this@DetailActivity){
                when(it){
                    is Resource.Success -> {
                        hideLoading()
                        val key = it.data?.get(0)?.key ?: ""

                        binding.btnPlayTrailer.setOnClickListener {
                            VideoPlayerDialogFragment.newInstance(
                                url = key
                            ).show(supportFragmentManager, VideoPlayerDialogFragment.TAG)
                        }
                    }
                    is Resource.Error -> {
                        hideLoading()
                        this@DetailActivity.shortToast(it.message ?: "Something went wrong")
                    }
                    is Resource.Loading -> {
                        showLoading()
                    }
                }
            }
        }
    }

    private fun setupDetailMovie(data: Movie?) {
        with(binding){
            tvTitle.text = data?.title
            tvOverview.text= data?.overview
            ivImgCourse.loadImage(
                BuildConfig.BACKDROP_PATH + data?.backdropImage
            )

            tvLanguage.text = data?.originalLanguage
            tvDuration.text = data?.runtime?.hourMinutes()
            tvRating.text = data?.voteAverage?.roundTo(1).toString()
            tvReleaseDate.text = data?.releaseDate

            tvMovieTitle.text = data?.title

            //region populate genre
            val genreList = data?.genres?.map {it.name}
            genreList?.forEach {
                val chip = createChip(it)
                chipGroup.addView(chip)
            }
            // end region populate genre
        }
    }

    private fun fetchViewModelCallback() {
        viewModel.apply {
            getDetailGame(movieId)
            getMovieVideos(movieId)
            lifecycleScope.launch {
                getMovieReview(movieId).collectLatest { reviews ->
                    reviewAdapter.submitData(reviews)
                }
            }
        }
    }

    private fun createChip(label: String): Chip {
        val chip = ViewGenreChipBinding.inflate(layoutInflater).root
        chip.text = label
        chip.isClickable = false
        return chip
    }

    private fun hideLoading() {
        if (progressDialog != null && progressDialog?.isShowing!!) {
            progressDialog?.cancel()
        }
    }

    private fun showLoading() {
        hideLoading()
        progressDialog = Utils.showLoadingDialog(this)
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}