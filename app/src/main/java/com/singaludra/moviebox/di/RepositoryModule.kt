package com.singaludra.moviebox.di

import com.singaludra.domain.repository.IMovieRepository
import com.singaludra.data.MovieRepository
import com.singaludra.data.remote.IRemoteDataSource
import com.singaludra.data.remote.RemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(includes = [NetworkModule::class])
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun provideRepository(movieRepository: MovieRepository): IMovieRepository

    @Binds
    abstract fun provideRemoteDataSource(remoteDataSource: RemoteDataSource): IRemoteDataSource
}