package com.singaludra.data

import androidx.paging.PagingData
import androidx.paging.map
import com.singaludra.domain.Resource
import com.singaludra.domain.model.Movie
import com.singaludra.domain.model.Review
import com.singaludra.domain.model.Video
import com.singaludra.domain.repository.IMovieRepository
import com.singaludra.data.remote.RemoteDataSource
import com.singaludra.data.remote.network.ApiResponse
import com.singaludra.data.remote.response.DetailMovieResponse
import com.singaludra.data.remote.response.GenreResponse
import com.singaludra.data.remote.response.VideosResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
): IMovieRepository {
    override fun getMovies(genreId: String?): Flow<PagingData<Movie>> {
        return remoteDataSource.getMovies(genreId)
            .map { pagingData ->
                pagingData.map { remoteMovie ->
                    remoteMovie.mapToDomain()
                }
            }
    }

    override fun getMovieGenre(): Flow<Resource<List<Movie.Genre>>> {
        return object : RepositoryLoader<GenreResponse, List<Movie.Genre>>(){
            override suspend fun createCall(): Flow<ApiResponse<GenreResponse>> {
                return remoteDataSource.getMovieGenre()
            }

            override fun mapApiResponseToDomain(data: GenreResponse): List<Movie.Genre> {
                return data.genres.map {
                    it.mapToDomain()
                }
            }
        }.asFlow()
    }

    override fun getDetailMovie(id: Int): Flow<Resource<Movie>> {
        return object : RepositoryLoader<DetailMovieResponse, Movie>(){
            override suspend fun createCall(): Flow<ApiResponse<DetailMovieResponse>> {
                return remoteDataSource.getDetailMovie(id)
            }

            override fun mapApiResponseToDomain(data: DetailMovieResponse): Movie {
                return data.mapToDomain()
            }
        }.asFlow()
    }

    override fun getMovieReviews(id: Int): Flow<PagingData<Review>> {
        return remoteDataSource.getMovieReviews(id)
            .map { pagingData ->
                pagingData.map { movieReview ->
                    movieReview.mapToDomain()
                }
            }
    }

    override fun getMovieVideos(id: Int): Flow<Resource<List<Video>>> {
        return object : RepositoryLoader<VideosResponse, List<Video>>(){
            override suspend fun createCall(): Flow<ApiResponse<VideosResponse>> {
                return remoteDataSource.getMovieVideos(id)
            }

            override fun mapApiResponseToDomain(data: VideosResponse): List<Video> {
                return data.results.map {
                    it.mapToDomain()
                }
            }
        }.asFlow()
    }
}