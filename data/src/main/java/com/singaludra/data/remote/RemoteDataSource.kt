package com.singaludra.data.remote

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.singaludra.data.remote.network.ApiResponse
import com.singaludra.data.remote.network.ApiService
import com.singaludra.data.remote.paging.MovieReviewPagingSource
import com.singaludra.data.remote.paging.PopularMoviePagingSource
import com.singaludra.data.remote.response.DetailMovieResponse
import com.singaludra.data.remote.response.GenreResponse
import com.singaludra.data.remote.response.MovieResponse
import com.singaludra.data.remote.response.ReviewResponse
import com.singaludra.data.remote.response.VideosResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

const val NETWORK_PAGE_SIZE = 1
class RemoteDataSource @Inject constructor(
    private val apiService: ApiService
): IRemoteDataSource {
    override fun getMovies(genreId: String?): Flow<PagingData<MovieResponse>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                PopularMoviePagingSource(apiService, genreId)
            }
        ).flow
    }

    override fun getMovieGenre(): Flow<ApiResponse<GenreResponse>> {
        return flow {
            try {
                val response = apiService.getGenreMovie()
                if (response != null) {
                    emit(ApiResponse.Success(response))
                } else {
                    emit(ApiResponse.Empty)
                }
            } catch (e : Exception){
                emit(ApiResponse.Error(e.parse()))
            }
        }.flowOn(Dispatchers.IO)
    }

    override fun getDetailMovie(id: Int): Flow<ApiResponse<DetailMovieResponse>> {
        return flow {
            try {
                val response = apiService.getDetailMovie(
                    id
                )
                if (response != null) {
                    emit(ApiResponse.Success(response))
                } else {
                    emit(ApiResponse.Empty)
                }
            } catch (e : Exception){
                emit(ApiResponse.Error(e.parse()))
            }
        }.flowOn(Dispatchers.IO)
    }

    override fun getMovieReviews(id: Int): Flow<PagingData<ReviewResponse>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                MovieReviewPagingSource(apiService, id)
            }
        ).flow
    }

    override fun getMovieVideos(id: Int): Flow<ApiResponse<VideosResponse>> {
        return flow {
            try {
                val response = apiService.getMovieVideos(
                    id
                )
                if (response != null) {
                    emit(ApiResponse.Success(response))
                } else {
                    emit(ApiResponse.Empty)
                }
            } catch (e : Exception){
                emit(ApiResponse.Error(e.parse()))
            }
        }.flowOn(Dispatchers.IO)
    }

    private fun Exception.parse(): String {
        return when(this){
            is IOException -> {
                "You have no connection!"
            }

            is HttpException -> {
                if(this.code() == 422){
                    "Invalid Data"
                } else {
                    "Invalid request"
                }
            }

            else -> {
                this.message.toString()
            }
        }
    }
}