package com.singaludra.data.remote.network

import com.singaludra.data.remote.response.DetailMovieResponse
import com.singaludra.data.remote.response.GenreResponse
import com.singaludra.data.remote.response.MovieResponse
import com.singaludra.data.remote.response.ResponseItems
import com.singaludra.data.remote.response.ReviewResponse
import com.singaludra.data.remote.response.VideosResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET("movie/popular")
    suspend fun getPopularMovie(
        @Query("page") page: Int,
        @Query("with_genres") genreId: String?,
    ): ResponseItems<MovieResponse>

    @GET("genre/movie/list")
    suspend fun getGenreMovie(): GenreResponse

    @GET("movie/{movie_id}")
    suspend fun getDetailMovie(
        @Path("movie_id") id: Int
    ): DetailMovieResponse

    @GET("movie/{movie_id}/reviews")
    suspend fun getMovieReviews(
        @Path("movie_id") id: Int,
        @Query("page") page: Int
    ): ResponseItems<ReviewResponse>

    @GET("movie/{movie_id}/videos")
    suspend fun getMovieVideos(
        @Path("movie_id") id: Int
    ): VideosResponse
}