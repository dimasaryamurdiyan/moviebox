package com.singaludra.data.remote.response

import com.google.gson.annotations.SerializedName
import com.singaludra.domain.model.Movie

data class MovieResponse(
    @SerializedName("id")
    var id: Int,
    @SerializedName("original_title")
    var originalTitle: String,
    @SerializedName("overview")
    var overview: String,
    @SerializedName("poster_path")
    var posterPath: String,
    @SerializedName("title")
    var title: String,
){
    fun mapToDomain(): Movie {
        return Movie(
            id = this.id,
            title = this.originalTitle ?: this.title,
            overview = this.overview,
            image = this.posterPath,
        )
    }
}