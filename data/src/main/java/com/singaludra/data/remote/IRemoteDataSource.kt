package com.singaludra.data.remote

import androidx.paging.PagingData
import com.singaludra.data.remote.network.ApiResponse
import com.singaludra.data.remote.response.DetailMovieResponse
import com.singaludra.data.remote.response.GenreResponse
import com.singaludra.data.remote.response.MovieResponse
import com.singaludra.data.remote.response.ReviewResponse
import com.singaludra.data.remote.response.VideosResponse
import kotlinx.coroutines.flow.Flow

interface IRemoteDataSource {
    fun getMovies(genreId: String?): Flow<PagingData<MovieResponse>>
    fun getMovieGenre(): Flow<ApiResponse<GenreResponse>>
    fun getDetailMovie(id: Int): Flow<ApiResponse<DetailMovieResponse>>
    fun getMovieReviews(id: Int): Flow<PagingData<ReviewResponse>>
    fun getMovieVideos(id: Int): Flow<ApiResponse<VideosResponse>>
}